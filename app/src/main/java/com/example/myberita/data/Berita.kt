package com.example.myberita.data

data class Berita (
    val id: Int,
    val judul: String,
    val gambar: String,
    val desc: String
        )